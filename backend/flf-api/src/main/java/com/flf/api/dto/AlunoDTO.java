package com.flf.api.dto;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.flf.api.model.Aluno;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @EqualsAndHashCode
@NoArgsConstructor @AllArgsConstructor @Builder
public class AlunoDTO {

	public AlunoDTO(Aluno aluno) {
		this.codigo = aluno.getCodigo();
		this.nome = aluno.getNome();
		this.cpf = aluno.getCpf();
		this.matricula = aluno.getMatricula();
	}
	
	public static List<AlunoDTO> returnListDTO(List<Aluno> array) {
		List<AlunoDTO> arrayRetorno = new ArrayList<AlunoDTO>();
		for (Aluno entity : array) {
			arrayRetorno.add(new AlunoDTO(entity));
		}
		
		return arrayRetorno;
	}
	
	private Integer codigo;
	private String nome;
	private String cpf;
	private String matricula;
	
	private Boolean alunoMatriculadoEmCurso;
	
	public void setAlunoMatriculadoEmCurso(BigInteger val) {
		this.alunoMatriculadoEmCurso = val.intValue() == 1 ? true : false;
	}
	
}
