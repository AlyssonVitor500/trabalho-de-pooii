package com.flf.api.controller;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.flf.api.business.CursoBusiness;
import com.flf.api.dto.AlunoDTO;
import com.flf.api.dto.CursoDTO;
import com.flf.api.exception.BusinessException;

@RestController
@RequestMapping("/cursos")
public class CursoController {

	@Autowired
	private CursoBusiness cursoBusiness;
	
	@GetMapping()
	public ResponseEntity<List<CursoDTO>> getAllCursos() {
		return ResponseEntity.ok(this.cursoBusiness.getAllCursos());
	}
	
	@GetMapping("/{codigo}")
	public ResponseEntity<CursoDTO> getCursoById(@PathVariable Integer codigo) {
		return ResponseEntity.ok(this.cursoBusiness.getCursoById(codigo));
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteCursoById(@PathVariable Integer codigo) {
		this.cursoBusiness.deleteCursoById(codigo);
	}
	
	@PostMapping()
	public ResponseEntity<CursoDTO> saveCurso(@RequestBody CursoDTO curso) {
		return ResponseEntity.status(HttpStatus.CREATED).body(this.cursoBusiness.saveCurso(curso));
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<CursoDTO> updateCurso(@PathVariable Integer codigo, @RequestBody CursoDTO curso) {
		curso.setCodigo(codigo);
		return ResponseEntity.status(HttpStatus.CREATED).body(this.cursoBusiness.update(curso));
	}
	
	@PostMapping("/cadastrar-aluno/{codigoCurso}/{codigoAluno}")
	@ResponseStatus(value = HttpStatus.CREATED)
	public void cadastrarAluno(@PathVariable Integer codigoCurso, @PathVariable Integer codigoAluno) throws BusinessException, SQLException {
		this.cursoBusiness.adicionarCurso(codigoCurso, codigoAluno);
	}
	
	@DeleteMapping("/deletar-aluno/{codigoCurso}/{codigoAluno}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void removerAluno(@PathVariable Integer codigoCurso, @PathVariable Integer codigoAluno) throws BusinessException, SQLException {
		this.cursoBusiness.removerAlunoDoCurso(codigoCurso, codigoAluno);
	}
	
	@GetMapping("/buscar-alunos/{codigoCurso}")
	public ResponseEntity<List<AlunoDTO>> retornarAlunosPorCurso(@PathVariable Integer codigoCurso) {
		return ResponseEntity.ok(this.cursoBusiness.retornarAlunosPorCurso(codigoCurso));
	}
	
	@GetMapping("/listar-todos-nao-cadastrado/{codigoCurso}")
	public ResponseEntity<List<AlunoDTO>> retornarAlunosNaoCadastradosNoCurso(@PathVariable Integer codigoCurso) {
		return ResponseEntity.ok(this.cursoBusiness.retornarAlunosNaoCadastradosNoCurso(codigoCurso));
	}
}
