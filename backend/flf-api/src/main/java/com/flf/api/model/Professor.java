package com.flf.api.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.flf.api.dto.ProfessorDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "professor")
@Getter @Setter @EqualsAndHashCode
@NoArgsConstructor @AllArgsConstructor @Builder
public class Professor {
	
	public Professor(ProfessorDTO professorDTO) {
		this.codigo = professorDTO.getCodigo();
		this.cpf = professorDTO.getCpf();
		this.nome = professorDTO.getNome();
		this.salario = professorDTO.getSalario();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_professor")
	private Integer codigo;
	
	@NotNull
	private String nome;
	
	@NotNull
	private String cpf;

	@NotNull
	private Double salario;
}
