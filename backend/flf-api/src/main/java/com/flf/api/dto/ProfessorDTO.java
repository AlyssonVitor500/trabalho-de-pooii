package com.flf.api.dto;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.flf.api.model.Professor;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @EqualsAndHashCode
@NoArgsConstructor @AllArgsConstructor @Builder
public class ProfessorDTO {

	public ProfessorDTO(Professor professor) {
		this.codigo = professor.getCodigo();
		this.nome = professor.getNome();
		this.cpf = professor.getCpf();
		this.salario = professor.getSalario();
	}
	
	public static List<ProfessorDTO> returnListDTO(List<Professor> array) {
		List<ProfessorDTO> returnList = new ArrayList<ProfessorDTO>();
		for (Professor entity : array) {
			returnList.add(new ProfessorDTO(entity));
		}
		
		return returnList;
	}
	
	private Integer codigo;
	private String nome;
	private String cpf;
	private Double salario;
	private boolean professorMinistraCurso;
	
	public void setProfessorMinistraCurso(BigInteger val) {
		this.professorMinistraCurso = val.intValue() == 1 ? true : false;
	}
}
