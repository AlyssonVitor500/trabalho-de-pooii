package com.flf.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.flf.api.business.ProfessorBusiness;
import com.flf.api.dto.ProfessorDTO;
import com.flf.api.exception.BusinessException;

@RestController
@RequestMapping("/professores")
public class ProfessorController {

	@Autowired 
	private ProfessorBusiness professorBusiness;
	
	
	@GetMapping
	public ResponseEntity<List<ProfessorDTO>> getAllProfessores() {
		return ResponseEntity.ok(professorBusiness.getAllProfessores());
	}
	
	@GetMapping("/{codigo}")
	public ResponseEntity<ProfessorDTO> getProfessorDTOByCodigo(@PathVariable Integer codigo) {
		return ResponseEntity.ok(this.professorBusiness.getProfessorDTOByCodigo(codigo));
	}
	
	@PostMapping
	public ResponseEntity<ProfessorDTO> saveProfessor(@RequestBody ProfessorDTO professor) throws BusinessException {
		return ResponseEntity.status(HttpStatus.CREATED).body(professorBusiness.saveProfessor(professor));
	}
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteProfessor(@PathVariable Integer codigo) {
		this.professorBusiness.deleteProfessorById(codigo);
	}
	
	@PutMapping("/{codigo}")
	public ResponseEntity<ProfessorDTO> updataProfessor(@PathVariable Integer codigo, @RequestBody ProfessorDTO professorDTO) throws BusinessException {
		professorDTO.setCodigo(codigo);
		return ResponseEntity.ok(professorBusiness.updateProfessor(professorDTO));
	}
}
