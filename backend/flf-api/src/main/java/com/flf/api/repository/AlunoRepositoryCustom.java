package com.flf.api.repository;

import java.util.List;

import com.flf.api.dto.AlunoDTO;

public interface AlunoRepositoryCustom {

	public List<AlunoDTO> getAllAlunosPorNome(String nome);
	
	public AlunoDTO getAlunoByCodigo(Integer codigo);
}	
