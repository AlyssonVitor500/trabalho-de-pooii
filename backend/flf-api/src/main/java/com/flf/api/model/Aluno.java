package com.flf.api.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.flf.api.dto.AlunoDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "aluno")
@Getter @Setter @EqualsAndHashCode
@NoArgsConstructor @AllArgsConstructor @Builder
public class Aluno {
	
	public Aluno(AlunoDTO aluno) {
		this.codigo = aluno.getCodigo();
		this.cpf = aluno.getCpf();
		this.nome = aluno.getNome();
		this.matricula = aluno.getMatricula();
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_aluno")
	private Integer codigo;
	
	@NotNull
	private String nome;
	
	@NotNull
	private String cpf;
	
	@NotNull
	private String matricula;
	
	@ManyToMany(mappedBy = "alunosMatriculados")
	private Collection<Curso> cursosMatriculados;

}
