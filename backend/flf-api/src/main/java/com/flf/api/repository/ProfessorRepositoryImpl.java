package com.flf.api.repository;

import java.util.List;

import org.hibernate.query.NativeQuery;
import org.hibernate.transform.AliasToBeanResultTransformer;

import com.flf.api.dto.ProfessorDTO;
import com.flf.api.model.Professor;

public class ProfessorRepositoryImpl extends GenericCustomRepositoryImpl<Professor, Integer> implements ProfessorRepositoryCustom {

	@Override
	public ProfessorDTO getProfessorDTOByCodigo(Integer codigo) {
		
		StringBuilder sql = new StringBuilder();
		
		sql.append(" SELECT p.cd_professor as \"codigo\", ");
		sql.append(" p.nome as \"nome\", p.cpf as \"cpf\",CONVERT(p.salario, double) as \"salario\" ,");
		sql.append(" CAST(case when (SELECT COUNT(1) FROM curso WHERE cd_professor = p.cd_professor) > 0 then true else false end AS UNSIGNED) as \"professorMinistraCurso\" ");
		sql.append(" FROM PROFESSOR p WHERE p.cd_professor = :codigo ");
		
		NativeQuery query = getSession().createNativeQuery(sql.toString());
		
		query.setParameter("codigo", codigo);
		
		query.setResultTransformer(new AliasToBeanResultTransformer(ProfessorDTO.class));
		
		return (ProfessorDTO) query.uniqueResult();
	}

	@Override
	public List<ProfessorDTO> getProfessorDTOList() {
		StringBuilder sql = new StringBuilder();
		
		sql.append(" SELECT p.cd_professor as \"codigo\", ");
		sql.append(" p.nome as \"nome\", p.cpf as \"cpf\", CONVERT (p.salario, double) as \"salario\" ,");
		sql.append(" CAST(case when (SELECT COUNT(1) FROM curso WHERE cd_professor = p.cd_professor) > 0 then 1 else 0 end AS UNSIGNED) as \"professorMinistraCurso\" ");
		sql.append(" FROM PROFESSOR p ");
		
		NativeQuery query = getSession().createNativeQuery(sql.toString());
		
		return query.setResultTransformer(new AliasToBeanResultTransformer(ProfessorDTO.class)).list();
	}  

}
