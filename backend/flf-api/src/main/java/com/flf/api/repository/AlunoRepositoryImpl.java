package com.flf.api.repository;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.query.NativeQuery;
import org.hibernate.transform.AliasToBeanResultTransformer;

import com.flf.api.dto.AlunoDTO;
import com.flf.api.model.Aluno;

@SuppressWarnings("deprecation")
public class AlunoRepositoryImpl extends GenericCustomRepositoryImpl<Aluno, Integer> implements AlunoRepositoryCustom {

	@Override
	public List<AlunoDTO> getAllAlunosPorNome(String nome) {
		
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT a.CD_ALUNO as \"codigo\", ");
		sql.append(" a.nome as \"nome\", ");
		sql.append(" a.cpf as \"cpf\", ");
		sql.append(" a.matricula as \"matricula\", ");
		sql.append(" CAST(case when (SELECT COUNT(1) FROM curso_aluno ca WHERE ca.cd_aluno = a.cd_aluno) > 0 then true else false end AS UNSIGNED) as \"alunoMatriculadoEmCurso\" ");
		sql.append(" FROM aluno a ");
		
		if (nome != null) {
			sql.append(" WHERE nome like :nome ");
		}
		
		NativeQuery query = getSession().createNativeQuery(sql.toString());
		
		if (nome != null) { 
			query.setParameter("nome", "%"+nome+"%");
		}
		
		query.setResultTransformer(new AliasToBeanResultTransformer(AlunoDTO.class));
		
		return query.list();
	}

	@Override
	public AlunoDTO getAlunoByCodigo(Integer codigo) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT a.CD_ALUNO as \"codigo\", ");
		sql.append(" a.nome as \"nome\", ");
		sql.append(" a.cpf as \"cpf\", ");
		sql.append(" a.matricula as \"matricula\", ");
		sql.append(" CAST(case when (SELECT COUNT(1) FROM curso_aluno ca WHERE ca.cd_aluno = a.cd_aluno) > 0 then true else false end AS UNSIGNED) as \"alunoMatriculadoEmCurso\" ");
		sql.append(" FROM aluno a ");
		
		sql.append(" WHERE cd_aluno = :codigo ");
		
		NativeQuery query = getSession().createNativeQuery(sql.toString());
		
		query.setParameter("codigo", codigo);
		query.setResultTransformer(new AliasToBeanResultTransformer(AlunoDTO.class));
		
		return (AlunoDTO) query.uniqueResult();
	}


}
