package com.flf.api.repository;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;

public abstract class GenericCustomRepositoryImpl<T, ID extends Serializable> {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	public Session getSession() {
		return this.entityManager.unwrap(Session.class);
	}

}
