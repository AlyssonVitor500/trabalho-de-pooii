package com.flf.api.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flf.api.dto.AlunoDTO;
import com.flf.api.exception.BusinessException;
import com.flf.api.model.Aluno;
import com.flf.api.repository.AlunoRepository;

@Service
public class AlunoBusiness {
	
	@Autowired private AlunoRepository alunoRepository;
	
	public AlunoDTO saveAluno(AlunoDTO alunoParaSalvar) throws BusinessException {
		Aluno alunoSalvo = null;
		
		try {
			alunoSalvo = alunoRepository.save(new Aluno(alunoParaSalvar));
		} 
		catch (Exception e) {
			if (e.getMessage().contains("aluno.cpf")) {
				throw new BusinessException("Já existe um aluno cadastrado com esse cpf.");
			}
			
			if (e.getMessage().contains("aluno.matricula")) {
				throw new BusinessException("Já existe um aluno cadastrado com essa matrícula.");
			}
			
			throw new BusinessException("Ocorreu um erro.");
		}
		
		return new AlunoDTO(alunoSalvo);
	}
	
	public List<AlunoDTO> getAllAlunos() {
		return alunoRepository.getAllAlunosPorNome(null);
	}
	
	public AlunoDTO updateAluno(AlunoDTO aluno) {
		return new AlunoDTO(alunoRepository.save(new Aluno(aluno)));
	}
	
	public void deleteAluno(Integer codigo) {
		alunoRepository.delete(new Aluno().builder()
					.codigo(codigo)
					.build());
	}
	
	public List<AlunoDTO> getAllAlunosPorNome(String nome) {
		return alunoRepository.getAllAlunosPorNome(nome);
	}
	
	public AlunoDTO getAlunoByCodigo(Integer codigo) {
		return alunoRepository.getAlunoByCodigo(codigo);
	}
	
	public Aluno getAlunoEntityByCodigo(Integer codigo) {
		return alunoRepository.findById(codigo).orElse(null);
	}
}
