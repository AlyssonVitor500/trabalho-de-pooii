package com.flf.api.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.flf.api.dto.AlunoDTO;
import com.flf.api.dto.CursoDTO;
import com.flf.api.exception.BusinessException;
import com.flf.api.model.Aluno;
import com.flf.api.model.Curso;
import com.flf.api.repository.CursoRepository;

@Service
public class CursoBusiness {
	
	@Autowired 
	private CursoRepository cursoRepository;
	
	@Autowired
	private AlunoBusiness alunoBusiness;

	public List<CursoDTO> getAllCursos() {
		return CursoDTO.returnListDTO(this.cursoRepository.findAll());
	}
	
	public CursoDTO getCursoById(Integer codigo) {
		Curso curso = this.cursoRepository.findById(codigo)
								.orElse(null);
		return curso != null ? new CursoDTO(curso) : null;
	}
	
	private Curso getCursoEntityById(Integer codigo) {
		Curso curso = this.cursoRepository.findById(codigo)
								.orElse(null);
		return curso;
	}

	public void deleteCursoById(Integer codigo) {
		cursoRepository.deleteById(codigo);
	}

	public CursoDTO saveCurso(CursoDTO curso) {
		Curso cursoParaSalvar = new Curso(curso);
		return new CursoDTO(cursoRepository.save(cursoParaSalvar));
	}
	
	public CursoDTO update(CursoDTO curso) {
		Curso cursoParaSalvar = new Curso(curso);
		
		Curso cursoBanco = this.getCursoEntityById(curso.getCodigo());
		
		cursoParaSalvar.setAlunosMatriculados(cursoBanco.getAlunosMatriculados());
		return new CursoDTO(cursoRepository.save(cursoParaSalvar));
	}

	@Transactional(rollbackFor =  Exception.class)
	public void adicionarCurso(Integer codigoCurso, Integer codigoAluno) throws BusinessException, SQLException {

		Curso curso = getCursoEntityById(codigoCurso);
		
		if (curso != null) {
			Aluno aluno = alunoBusiness.getAlunoEntityByCodigo(codigoAluno);
			
			if (aluno != null) {
				curso.getAlunosMatriculados().add(aluno);
				cursoRepository.save(curso);
			}else {
				throw new BusinessException("Aluno inexistente!");
			}
		}else {
			throw new BusinessException("Curso inexistente!");
		}
	}
	
	@Transactional(rollbackFor =  Exception.class)
	public void removerAlunoDoCurso(Integer codigoCurso, Integer codigoAluno) throws BusinessException, SQLException {

		Curso curso = getCursoEntityById(codigoCurso);
		
		if (curso != null) {
			Aluno aluno = alunoBusiness.getAlunoEntityByCodigo(codigoAluno);
			
			if (aluno != null) {
				curso.getAlunosMatriculados().remove(aluno);
				cursoRepository.save(curso);
			}else {
				throw new BusinessException("Aluno inexistente!");
			}
		}else {
			throw new BusinessException("Curso inexistente!");
		}
	}

	public List<AlunoDTO> retornarAlunosPorCurso(Integer codigoCurso) {
		Curso curso = getCursoEntityById(codigoCurso);
		
		return curso != null && curso.getAlunosMatriculados() != null && !curso.getAlunosMatriculados().isEmpty()
				? AlunoDTO.returnListDTO(new ArrayList(curso.getAlunosMatriculados())) : null;
	}

	
	public List<AlunoDTO> retornarAlunosNaoCadastradosNoCurso(Integer codigoCurso) {
		Curso curso = getCursoEntityById(codigoCurso);
		
		List<AlunoDTO> alunosRetorno = new ArrayList<AlunoDTO>();
		
		for (AlunoDTO aluno : alunoBusiness.getAllAlunos()) {
			boolean alunoJaCadastrado = false;
			for (Aluno aluno2 : curso.getAlunosMatriculados()) {
				if (aluno2.getCodigo().equals(aluno.getCodigo())) {
					alunoJaCadastrado = true;
					
					break;
				}
			}
			
			if (!alunoJaCadastrado) {
				alunosRetorno.add(aluno);
			}
		}
		
		return alunosRetorno.stream().sorted(Comparator.comparing(AlunoDTO::getNome)).collect(Collectors.toList());
	}
}
