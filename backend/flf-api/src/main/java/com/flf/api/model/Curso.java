package com.flf.api.model;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.flf.api.dto.CursoDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "curso")
@Getter @Setter @EqualsAndHashCode
@NoArgsConstructor @AllArgsConstructor @Builder
public class Curso {
	
	public Curso(CursoDTO curso) {
		this.codigo = curso.getCodigo();
		this.nome = curso.getNome();
		this.professorCurso = new Professor(curso.getProfessor());
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cd_curso")
	private Integer codigo;
	
	@NotNull
	private String nome;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="cd_professor")
	private Professor professorCurso;
	
	@ManyToMany
	@JoinTable(
		name = "curso_aluno",
		joinColumns = @JoinColumn(name = "cd_curso"),
		inverseJoinColumns = @JoinColumn(name = "cd_aluno")
	)
	private Collection<Aluno> alunosMatriculados;

}
