package com.flf.api.repository;

import java.util.List;

import com.flf.api.dto.ProfessorDTO;

public interface ProfessorRepositoryCustom {

	public ProfessorDTO getProfessorDTOByCodigo(Integer codigo);
	
	public List<ProfessorDTO> getProfessorDTOList();
}
