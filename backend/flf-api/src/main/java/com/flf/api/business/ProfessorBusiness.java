package com.flf.api.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.flf.api.dto.ProfessorDTO;
import com.flf.api.exception.BusinessException;
import com.flf.api.model.Professor;
import com.flf.api.repository.ProfessorRepository;

@Service
public class ProfessorBusiness {

	@Autowired 
	private ProfessorRepository professorRepository;
	
	public List<ProfessorDTO> getAllProfessores() {
		return professorRepository.getProfessorDTOList();
	}
	
	public ProfessorDTO saveProfessor(ProfessorDTO professorToSave) throws BusinessException {
		Professor professor = null;
		try {
			professor = professorRepository.save(new Professor(professorToSave));
		} catch (Exception e) {
			if (e.getMessage().contains("professor.cpf")) {
				throw new BusinessException("Já existe um professor cadastrado com esse cpf.");
			}
			
			throw new BusinessException("Ocorreu um erro.");
		}
		
		return new ProfessorDTO(professor);
	}
	
	public ProfessorDTO updateProfessor(ProfessorDTO professorParaAtualizar) throws BusinessException {
		ProfessorDTO professorBanco = this.professorRepository.getProfessorDTOByCodigo(professorParaAtualizar.getCodigo());
		if (professorBanco == null) {
			throw new BusinessException("Profissional não existe.");
		}
		
		ProfessorDTO professor  = null;
		
		try {
			professor = new ProfessorDTO(professorRepository.save(new Professor(professorParaAtualizar)));
		} catch (Exception e) {
			if (e.getMessage().contains("professor.cpf")) {
				throw new BusinessException("Já existe um profissional cadastrado com esse cpf.");
			}
			
			throw new BusinessException("Ocorreu um erro.");
		}
		
		return getProfessorDTOByCodigo(professor.getCodigo());
	}
	
	public ProfessorDTO getProfessorDTOByCodigo(Integer codigo) {
		return professorRepository.getProfessorDTOByCodigo(codigo);
	}
	
	public void deleteProfessorById(Integer codigo) {
		professorRepository.deleteById(codigo);
	}
}
