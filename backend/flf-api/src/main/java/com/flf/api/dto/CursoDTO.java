package com.flf.api.dto;

import java.util.ArrayList;
import java.util.List;

import com.flf.api.model.Curso;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter @EqualsAndHashCode
@NoArgsConstructor @AllArgsConstructor @Builder
public class CursoDTO {
	
	public CursoDTO(Curso curso) {
		this.codigo = curso.getCodigo();
		this.nome = curso.getNome();
		this.professor = new ProfessorDTO(curso.getProfessorCurso());
		
		if (curso.getAlunosMatriculados() != null && curso.getAlunosMatriculados().size() > 0) {
			this.existeAlunosMatriculados = true;
			this.qtdAlunosMatriculados = curso.getAlunosMatriculados().size();
		}else {
			this.existeAlunosMatriculados = false;
			this.qtdAlunosMatriculados = 0;
		}
	}
	
	public static List<CursoDTO> returnListDTO(List<Curso> array) {
		List<CursoDTO> arrayRetorno = new ArrayList<CursoDTO>();
		for (Curso entity : array) {
			arrayRetorno.add(new CursoDTO(entity));
		}
		
		return arrayRetorno;
	}

	private Integer codigo;
	private String nome;
	private ProfessorDTO professor;
	private boolean existeAlunosMatriculados;
	private Integer qtdAlunosMatriculados;
	private List<AlunoDTO> alunosMatriculados;
	
}
