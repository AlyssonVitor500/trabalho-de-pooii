package com.flf.api.exception;

public class BusinessException extends Exception{

	public BusinessException(String msg) {
		super(msg);
	}
}
