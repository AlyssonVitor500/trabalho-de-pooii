CREATE TABLE IF NOT EXISTS aluno (
	cd_aluno INTEGER PRIMARY KEY AUTO_INCREMENT,
    nome varchar(150) not null,
    cpf varchar (11) not null unique,
    matricula varchar(11) not null unique
)  ENGINE=InnoDB DEFAULT CHARSET=utf8;