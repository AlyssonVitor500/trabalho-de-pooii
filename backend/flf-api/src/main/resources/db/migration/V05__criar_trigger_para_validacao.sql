DELIMITER $
CREATE TRIGGER trigger_curso_aluno BEFORE INSERT 
ON curso_aluno 
for each row
BEGIN
	
    DECLARE size INT;
    DECLARE msg varchar(128);
    
    SELECT COUNT(1)
		INTO size
        FROM curso_aluno
        WHERE cd_curso = new.cd_curso;
        
	IF size >= 5 then
		set msg = 'Erro ao inserir o aluno , pois o curso já possui o máximo de alunos cadastrados.';
        signal sqlstate '45000' set message_text = msg;
	END IF;
    
END$
    
DELIMITER ;