CREATE TABLE IF NOT EXISTS curso (
	cd_curso INTEGER PRIMARY KEY AUTO_INCREMENT,
    nome varchar(150) not null,
    cd_professor INTEGER not null,
    FOREIGN KEY (cd_professor) REFERENCES professor(cd_professor)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;