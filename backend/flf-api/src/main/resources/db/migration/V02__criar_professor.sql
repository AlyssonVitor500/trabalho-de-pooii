CREATE TABLE IF NOT EXISTS professor (
	cd_professor INTEGER PRIMARY KEY AUTO_INCREMENT,
    nome varchar(150) not null,
    cpf varchar(11) not null unique,
    salario DECIMAL(8, 2) not null
) ENGINE=InnoDB DEFAULT CHARSET=utf8;