CREATE TABLE IF NOT EXISTS curso_aluno (
	cd_curso_aluno INTEGER PRIMARY KEY AUTO_INCREMENT,
	cd_curso INTEGER NOT NULL,
    cd_aluno INTEGER NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE curso_aluno
ADD FOREIGN KEY (cd_aluno) REFERENCES aluno(cd_aluno);

ALTER TABLE curso_aluno
ADD FOREIGN KEY (cd_curso) REFERENCES curso(cd_curso);

CREATE UNIQUE INDEX tb_curso_aluno_cd_curso_um_cd_aluno_dois_uindex
	ON curso_aluno (cd_aluno, cd_curso);