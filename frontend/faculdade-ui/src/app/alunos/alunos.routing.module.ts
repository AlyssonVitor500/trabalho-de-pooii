import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlunosCadastroComponent } from './alunos-cadastro/alunos-cadastro.component';
import { AlunosPesquisaComponent } from './alunos-pesquisa/alunos-pesquisa.component';

const routes: Routes = [
  { path: 'alunos', component: AlunosPesquisaComponent },
  { path: 'alunos/novo', component: AlunosCadastroComponent },
  { path: 'alunos/:codigo', component: AlunosCadastroComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ]
})
export class AlunosRoutingModule { }
