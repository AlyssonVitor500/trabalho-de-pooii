import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { AlunoDTO } from 'src/app/core/model';
import { AlunoService } from '../aluno.service';

@Component({
  selector: 'app-alunos-cadastro',
  templateUrl: './alunos-cadastro.component.html',
  styleUrls: ['./alunos-cadastro.component.css']
})
export class AlunosCadastroComponent implements OnInit {

  alunoDTO: AlunoDTO = new AlunoDTO();

  constructor( private alunoService: AlunoService,
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private title: Title) { }

  ngOnInit(): void {
    this.title.setTitle('Novo Professor');

    const codigoAluno = this.route.snapshot.params['codigo'];

    if (codigoAluno) {
      this.carregarAluno(codigoAluno);
    }
  }

  salvar() {
    if(this.editando) {
      this.editarAluno();
    }else {
      this.inserirNovoAluno();
    }
  }


  retornarObjParaEnvio(): Object{
    const obj = {
      nome: this.alunoDTO.nome,
      matricula: this.alunoDTO.matricula,
      cpf: this.alunoDTO.cpf,
      codigo: null
    }

    if (this.editando) {
      obj.codigo = this.alunoDTO.codigo;
    }

    return obj;
  }

  editarAluno() {
    this.alunoService.editarAluno(this.alunoDTO.codigo,this.retornarObjParaEnvio())
      .subscribe(() => {
        this.messageService.add({severity: 'success', detail: 'Aluno alterado com sucesso!'})
        this.router.navigate(['/alunos']);
      })
  }

  inserirNovoAluno() {
    this.alunoService.salvarNovoAluno(this.retornarObjParaEnvio())
      .subscribe(res => {
        this.messageService.add({severity: 'success', detail: 'Aluno adicionado com sucesso!'})
        this.router.navigate(['/alunos', res.codigo])
      },
      err =>
      {
        this.messageService.add({severity: 'error', detail: err.error.message})
      });
  }

  carregarAluno(codigoAluno: number) {
    this.alunoService.getAlunoPorId(codigoAluno)
      .subscribe(res => {
        this.alunoDTO = res;
        this.atualizarTituloPagina();
      });
  }

  get editando() {
    return Boolean(this.alunoDTO.codigo)
  }

  novo() {
    this.router.navigate(['alunos/novo']);
  }

  atualizarTituloPagina() {
    this.title.setTitle(`Edição de aluno: ${this.alunoDTO.nome}`);
  }
}
