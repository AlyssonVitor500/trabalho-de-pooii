import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ConfirmationService, MessageService } from 'primeng/api';
import { AlunoDTO } from 'src/app/core/model';
import { AlunoService } from '../aluno.service';

@Component({
  selector: 'app-alunos-pesquisa',
  templateUrl: './alunos-pesquisa.component.html',
  styleUrls: ['./alunos-pesquisa.component.css']
})
export class AlunosPesquisaComponent implements OnInit {

  alunos: AlunoDTO[] = [];

  constructor(private alunoService: AlunoService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private title: Title) { }

  ngOnInit(): void {
    this.title.setTitle('Pesquisa de Alunos');
    this.pesquisar();
  }

  pesquisar() {
    this.alunoService.listarTodos()
      .subscribe(res => {
        this.alunos = res;
      })
  }

  confirmarExclusao(aluno) {
    this.confirmationService.confirm({
      message: 'Tem certeza que deseja excluir?',
      accept: () => {
        this.excluir(aluno);
      }
    })
  }

  excluir(aluno) {
    this.alunoService.deletar(aluno.codigo)
      .subscribe(() => {
        this.pesquisar();
      },
      err => {
        this.messageService.add({severity: 'error', detail: 'Não foi possível deletar aluno. Possivelmente ele está cadastrado em um curso. Desvincule-o do curso para excluir.', life: 5000})
      })
  }

}
