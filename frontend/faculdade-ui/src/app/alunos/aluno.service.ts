import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AlunoService {

  private cursosUrl = 'http://localhost:8080/alunos';

  constructor(private http: HttpClient) { }

  listarTodos(): Observable<any> {
    return this.http.get<any>(this.cursosUrl).pipe(
      map((obj) => obj)
    )
  }

  deletar(codigo: number): Observable<any> {
    return this.http.delete<any>(`${this.cursosUrl}/${codigo}`)
      .pipe();
  }

  getAlunoPorId(codigo: number): Observable<any> {
    return this.http.get<any>(`${this.cursosUrl}/${codigo}`)
      .pipe();
  }

  salvarNovoAluno(aluno): Observable<any>{
    return this.http.post(this.cursosUrl, aluno)
      .pipe(map(res => res));
  }

  editarAluno(alunoCodigo, aluno): Observable<any>{
    return this.http.put(`${this.cursosUrl}/${alunoCodigo}`, aluno)
      .pipe(map(res => res));
  }
}
