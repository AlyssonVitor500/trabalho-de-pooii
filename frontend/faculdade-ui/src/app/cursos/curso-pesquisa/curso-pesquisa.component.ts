import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { CursoDTO } from 'src/app/core/model';
import { CursoService } from '../curso.service';

@Component({
  selector: 'app-curso-pesquisa',
  templateUrl: './curso-pesquisa.component.html',
  styleUrls: ['./curso-pesquisa.component.css']
})
export class CursoPesquisaComponent implements OnInit {

  cursos: CursoDTO[] = [];
  @ViewChild('tabela') grid!: Table;
  constructor(
    private cursoService: CursoService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private title: Title
  ) { }

  ngOnInit(): void {
    this.title.setTitle('Pesquisa de Cursos');
    this.pesquisar();
  }

  pesquisar(): void {
    this.cursoService.buscarTodos().subscribe(cursos => {
      this.cursos = cursos;
    });
  }

  confirmarExclusao(curso: CursoDTO): void {
    this.confirmationService.confirm({
      message: 'Tem certeza que deseja excluir?',
      accept: () => {
          this.excluir(curso);
      }
    });
  }

  excluir(curso: any) {
    this.cursoService.excluir(curso.codigo)
      .then(() => {
          this.pesquisar();
          this.messageService.add({ severity: 'success', detail: 'Curso excluído com sucesso!' })
      })
  }

  returnStatusVagas(nVagas: number): string {
    if (nVagas === 5) {
      return 'Vagas totalmente abertas';
    }

    if (nVagas === 0) {
      return 'Vagas totalmente fechadas';
    }

    return 'Vagas parcialmente abertas'
  }

}
