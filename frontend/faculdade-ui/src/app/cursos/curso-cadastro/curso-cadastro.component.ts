import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { AlunoService } from 'src/app/alunos/aluno.service';
import { ErrorHandlerService } from 'src/app/core/error-handler.service';
import { AlunoDTO, CursoDTO, ProfessorDTO } from 'src/app/core/model';
import { ProfessorService } from 'src/app/professores/professor.service';
import { CursoService } from '../curso.service';

@Component({
  selector: 'app-curso-cadastro',
  templateUrl: './curso-cadastro.component.html',
  styleUrls: ['./curso-cadastro.component.css']
})
export class CursoCadastroComponent implements OnInit {

  curso: CursoDTO = new CursoDTO();
  professor: ProfessorDTO[] = [];

  aluno: AlunoDTO = new AlunoDTO();
  alunos: AlunoDTO[] = [];
  alunosSelection: AlunoDTO[] = [];

  constructor(
    private professorService: ProfessorService,
    private cursoService: CursoService,
    private errorHandler: ErrorHandlerService,
    private route: ActivatedRoute,
    private router: Router,
    private messageService: MessageService,
    private title: Title,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit(): void {
    this.title.setTitle('Novo Curso');
    const codigoCurso = this.route.snapshot.params['codigo'];

    this.carregarProfessores()
    if (codigoCurso) {
      this.carregarCurso(codigoCurso)
    }
  }

  get editando() {
    return Boolean(this.curso.codigo)
  }

  carregarCurso(codigo: number) {
    this.cursoService.buscarPorCodigo(codigo)
      .subscribe(curso => {
        this.curso = curso;
        this.carregarAlunosPorCurso();
      },
      erro => this.errorHandler.handle(erro));
  }

  carregarProfessores() {
    return this.professorService.listarTodos()
    .subscribe(professor => {
      this.professor = professor
          .map((p:any) => ({ label: p.nome, value: p.codigo }));
      }, erro => {
        this.errorHandler.handle(erro)
      })
  }

  salvar(form: NgForm) {
    if (this.editando) {
      this.atualizarCurso(form)
    } else {
      this.adicionarCurso(form)
    }
  }

  atualizarCurso(form: NgForm) {
    const obj = this.curso;
    const idObj = this.curso.professor.codigo;

    obj.professor = new ProfessorDTO();
    obj.professor.codigo = idObj;

    this.cursoService.atualizar(obj)
      .subscribe((curso: CursoDTO) => {
          this.messageService.add({ severity: 'success', detail: 'Curso alterado com sucesso!' });
          this.router.navigate(['/cursos']);
        },
        error => {this.messageService.add({ severity: 'error', detail: error.error.message })});
  }

  adicionarCurso(form: NgForm) {
    this.cursoService.salvar(this.curso).subscribe(
      (cursoAdicionado) => {
        this.messageService.add({ severity: 'success', detail: 'Curso adicionado com sucesso!' });
        this.router.navigate(['/cursos', cursoAdicionado.codigo]);
      },
      (error) => {
        this.messageService.add({ severity: 'error', detail: error.error.message });
      });
  }

  novo(cursoForm: NgForm) {
    cursoForm.reset(new CursoDTO);

    this.router.navigate(['cursos/novo']);
  }

  private atualizarTituloEdicao() {
    this.title.setTitle(`Edição de cursos: ${this.curso.nome}`);
  }

  confirmarExclusaoAluno(aluno: AlunoDTO) {
    this.confirmationService.confirm({
      message: 'Tem certeza que deseja desvincular o aluno a esse curso?',
      accept: () => {
          this.cursoService.deletarMatriculaAlunoNoCurso(this.curso.codigo, aluno.codigo)
            .subscribe(() => {
              this.messageService.add({ severity: 'success', detail: 'Aluno removido com sucesso!' });
              this.carregarAlunosPorCurso();
            })
      }
    });
  }

  adicionarAluno() {
    this.cursoService.adicionarAlunoNoCurso(this.aluno.codigo, this.curso.codigo)
      .subscribe(() => {
        this.messageService.add({ severity: 'success', detail: 'Aluno adicionado no curso com sucesso!' });
        this.carregarAlunosPorCurso();
        this.aluno = new AlunoDTO();
      },
      (error) => {
        if (this.alunos.length === 5) {
          this.messageService.add({ severity: 'error', detail: 'O máximo de cinco alunos já foi atingido!' });
        }else {
          this.messageService.add({ severity: 'error', detail: 'Erro ao adicionar!' });
       }
      })
  }

  carregarAlunosPorCurso(){
    this.cursoService.getAllAlunosPorCurso(this.curso.codigo)
      .subscribe(res => {
        this.alunos = res;
        this.carregarTodosAlunosNaoCadastradoNoCurso();
      })
  }

  get isAdicionar() {
    return Boolean(this.aluno.codigo);
  }


  carregarTodosAlunosNaoCadastradoNoCurso() {
    this.cursoService.listarTodosNaoCadastradosNoCurso(this.curso.codigo)
      .subscribe(alunos => {
        this.alunosSelection = alunos
        .map(p => ({ label: `${p.nome} (${p.matricula})`, value: p.codigo }));
    });
  }

}
