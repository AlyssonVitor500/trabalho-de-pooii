import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { CursoDTO } from '../core/model';

@Injectable({
  providedIn: 'root'
})
export class CursoService {

  cursosUrl = 'http://localhost:8080/cursos';

  constructor(private http: HttpClient) { }

  buscarTodos(): Observable<any> {
    return this.http.get<any>(this.cursosUrl).pipe(
      map((obj) => obj)
    )
  }

  salvar(curso: any): Observable<any> {
    return this.http.post<any>(this.cursosUrl, curso).pipe(
      map((obj) => obj)
    );
  }

  atualizar(curso: any): Observable<any> {
    return this.http.put<any>(`${this.cursosUrl}/${curso.codigo}`, curso, {  })
      .pipe(map((obj) => obj));
  }

  excluir(codigo: number): Promise<void> {
    return this.http.delete<void>(`${this.cursosUrl}/${codigo}`, {  })
      .toPromise();
  }

  buscarPorCodigo(codigo: number): Observable<any> {
    return this.http.get(`${this.cursosUrl}/${codigo}`, {  })
      .pipe(map((obj) => obj));
  }

  adicionarAlunoNoCurso(codigoAluno: number, codigoCurso: number) {
    return this.http.post<any>(`${this.cursosUrl}/cadastrar-aluno/${codigoCurso}/${codigoAluno}`, {}).pipe(
      map(() => {})
    );
  }

  getAllAlunosPorCurso(codigoCurso: number) {
    return this.http.get<any>(`${this.cursosUrl}/buscar-alunos/${codigoCurso}`, {}).pipe(
      map((alunos) => alunos)
    );
  }

  listarTodosNaoCadastradosNoCurso(codigoCurso: number): Observable<any> {
    return this.http.get<any>(`${this.cursosUrl}/listar-todos-nao-cadastrado/${codigoCurso}`, {})
      .pipe(map((aluno) => aluno));
  }

  deletarMatriculaAlunoNoCurso(codigoCurso: number, codigoAluno: number): Observable<any> {
    return this.http.delete<any>(`${this.cursosUrl}/deletar-aluno/${codigoCurso}/${codigoAluno}`, {})
      .pipe(map(() => {}));
  }
}
