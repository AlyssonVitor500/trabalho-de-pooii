import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProfessorDTO } from '../core/model';

@Injectable({
  providedIn: 'root'
})
export class ProfessorService {

  professorUrl = 'http://localhost:8080/professores';

  constructor(private http: HttpClient) { }

  listarTodos(): Observable<any> {
    return this.http.get<any>(this.professorUrl, {  })
    .pipe(map((obj) => obj));
  }

  retornarTodosProfessores(): Observable<any> {
    return this.http.get<any>(this.professorUrl).pipe(
      map((obj) => obj)
    )
  }

  criarProfessor(professor: any): Observable<any> {
    return this.http.post<any>(this.professorUrl, professor).pipe(
      map((obj) => obj)
    );
  }

  atualizar(professor: any): Observable<any> {
    return this.http.put<any>(`${this.professorUrl}/${professor.codigo}`, professor, {  })
      .pipe(map(res => res));
  }

  excluir(codigo: number): Observable<void> {
    return this.http.delete<void>(`${this.professorUrl}/${codigo}`, {  })
      .pipe();
  }

  buscarPorCodigo(codigo: number): Promise<any> {
    return this.http.get(`${this.professorUrl}/${codigo}`)
      .toPromise()
      .then(response => {
        const professor = response;
        return professor;
      });
  }
}
