import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { ProfessorDTO } from 'src/app/core/model';
import { ProfessorService } from '../professor.service';

@Component({
  selector: 'app-professor-pesquisa',
  templateUrl: './professor-pesquisa.component.html',
  styleUrls: ['./professor-pesquisa.component.css']
})
export class ProfessorPesquisaComponent implements OnInit {

  professores: ProfessorDTO[] = [];

  constructor(
    private professorService: ProfessorService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private title: Title
  ) { }

  ngOnInit(): void {
    this.title.setTitle('Pesquisa de Professores');
    this.pesquisar();
  }

  pesquisar(): void {
    this.professorService.retornarTodosProfessores().subscribe(prof => {
      this.professores = prof;
    });
  }

  confirmarExclusao(professor: ProfessorDTO): void {
    this.confirmationService.confirm({
      message: 'Tem certeza que deseja excluir?',
      accept: () => {
          this.excluir(professor);
      }
    });
  }

  excluir(professor: any) {
    this.professorService.excluir(professor.codigo)
      .subscribe(() => {
        this.pesquisar();
        this.messageService.add({ severity: 'success', detail: 'Professor excluído com sucesso!' })
      },
      err => {
        this.messageService.add({severity: 'error', detail: 'Não foi possível deletar professor. Possivelmente ele ministra algum curso. Desvincule-o do curso para excluir.', life: 5000})
      })
  }

}
