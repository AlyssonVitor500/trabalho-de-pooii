export class AlunoDTO {
  codigo: number;
  nome: string;
  cpf: string;
  matricula: string;
  alunoMatriculadoEmCurso: boolean = false;
}

export class ProfessorDTO {
  codigo: number;
  nome: string;
  cpf: string;
  salario: number;
  professorMinistraCurso;
}

export class CursoDTO {
  codigo: number;
  nome: string;
  professor: ProfessorDTO = new ProfessorDTO();
  existeAlunosMatriculados: boolean;
  qtdAlunosMatriculados: number;
  alunosMatriculados: AlunoDTO[];
}
