import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent{

  exibindoMenu: boolean = false;

  trocarValorMenu() {
    this.exibindoMenu = !this.exibindoMenu;
  }

}
