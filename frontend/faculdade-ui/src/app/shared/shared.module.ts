import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageComponent } from './message/message.component';
import { SimOuNaoPipe } from './sim-ou-nao.pipe';
import { CpfTransformPipe } from './cpf-transform.pipe';



@NgModule({
  declarations: [
    MessageComponent,
    SimOuNaoPipe,
    CpfTransformPipe,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MessageComponent,
    SimOuNaoPipe,
    CpfTransformPipe,
  ]
})
export class SharedModule { }
