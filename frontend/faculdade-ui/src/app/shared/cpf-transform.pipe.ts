import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cpfTransform'
})
export class CpfTransformPipe implements PipeTransform {

  transform(value: string): string {
    return value.length === 11 ? `${value.slice(0,3)}.${value.slice(3,6)}.${value.slice(6,9)}-${value.slice(9,11)}` : value;
  }

}
